#include<stdio.h>
void swap(int *a,int *b)
{
 int temp;
 temp=*a;
 *a=*b;
 *b=temp;
}
int main()
{ 
int n1,n2;
printf("Enter numbers\n");
scanf("%d%d",&n1,&n2);
printf("Values before swapping are%d and %d\n",n1,n2);
swap(&n1,&n2);
printf("Values after swapping are %d and %d\n",n1,n2);
return 0;}
